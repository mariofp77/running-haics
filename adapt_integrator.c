/*
 * Author/s:
 *  Tijana Radivojevic
 *  Felix Müller
 *  Jorge Pérez
 */

#include <string.h>
#include <math.h>

#include "Globals.h"
#include "Definitions.h"

void fnPrintErrorScalingNotYetImplemented(int scaling_method, char *model, char *data){

  fprintf(stderr, "adapt_integrator.c - Scaling method '%i' not yet implemented for model '%s' and dataset '%s'. Exiting.\n", scaling_method, model, data);
}

void fnPrintErrorDatasetNotFound(int scaling_method, char *model, char *data){

  fprintf(stderr, "adapt_integrator.c - fnCalculateScaling: Dataset '%s' not found. Scaling method: '%i', model: '%s'.\n", data, scaling_method, model);
}

void fnPrintErrorModelNotFound(int scaling_method, char *model, char *data){

  fprintf(stderr, "adapt_integrator.c - fnCalculateScaling: Model '%s' not found. Scaling method: '%i', dataset: '%s'.\n", model, scaling_method, data);
}

// maximize rho for a given b on (0, ss_scaled)
static double rho_function(double ss_scaled, double b) {

  double ss2, ss4;
  double ss_incr = 0.001;
  double aux, aux1, rhoAux;
  double rho = 0;
  double ss_trial = -ss_incr;

  if (strcmp(g_inpset->integrator, "2sAIA-HMC") == 0) {
    while (ss_trial < ss_scaled) {
      ss_trial += ss_incr;
      ss2 = ss_trial*ss_trial;
      ss4 = ss2*ss2;

      aux1 = (1-2*b)*ss2;
      aux = 6 - b*(4 + aux1);
      aux = 1 - b*aux;
      aux = ss4 * aux*aux * 1000;
      aux = aux/(2-b*ss2)/2;
      aux = aux/(4 - aux1);
      rhoAux = aux/(2 - b*aux1);

      if (rhoAux > rho)
        rho = rhoAux;
    }

  } else if (strcmp(g_inpset->integrator, "2sAIA-MMHMC") == 0) {
    while (ss_trial < ss_scaled) {
      ss_trial += ss_incr;
      ss2 = ss_trial*ss_trial;
      ss4 = ss2*ss2;

      aux1 = (1-2*b)*ss2;
      aux = 6 - b*(4 + aux1);
      aux = 1 - b*aux;
      aux = ss4*ss2*aux*(2 - b*(12 + 4*b*(6*b-5) + b*(1 + 4*b*(3*b-2))*ss2)) * 1000;
      aux = aux/(2-b*ss2)/24;
      aux = aux/(4 - aux1);
      rhoAux = aux/(2 - b*aux1);

      if (rhoAux > rho)
        rho = rhoAux;
    }
  }

  return rho;
}

struct opt_rho {
  double rho;
  double b;
};

// scale user given time step
static double scale_timestep(double ss) {

  double ss_scaled = scaling * w_max * ss;

  return ss_scaled;

}

// find b that minimizes max rho
static struct opt_rho rho_optimization_2S(double ss_scaled, double rho2) {

  struct opt_rho result;
  double b2   = 0.25;//1sVV integrator
  double b, b_opt;
  double rho1 = 0.;

  b = 0;
  b_opt = b2;

  while (b <= b2) {
    b += 1e-6;

    rho1 = rho_function(ss_scaled, b);

    if (rho1 < rho2) {
      rho2 = rho1;
      b_opt = b;
    }
  }

  result.rho = rho2;
  result.b = b_opt;
  return result;
}

double FindIntegrator(double *b_coeff, double ss, int num_integrator_stages) {
  double ss_scaled, b;
  int ind;


  //this check is very crude, because for statistics it is unclear what would be a good value to check - thus it will get commented out for now
  //if (ss > sqrt(2)/w_max) { //this check corresponds e.g. the orders from table 3.1 from thesis by mario fernandez
  //fprintf(stderr, "\nWARNING: FindIntegrator function: Unscaled timestep exceeds the safety limit.\n");
  //}

  ss_scaled = scale_timestep(ss);

#ifdef DEBUG
  fprintf(lfp, "timestep: %lf\n", ss);
  fprintf(lfp, "Scaled timestep: %lf\n", ss_scaled);
#endif

  // the mapping "h_bar to b" is tabulated outside of haics already (it is read into b_aia array).
  // TWO STAGE (stability limit 4)
  // if scaled timestep (ss_scaled) is above stability limit (here 4) issue a warning and set b to max. value (which is 0.25)
  // if ss_scaled is in (0,4) look it up in the table
  // THREE STAGE (stability limit 6)
  // if scaled timestep (ss_scaled) is above stability limit (here 6) issue a warning and set b to max. value (which is 0.25)
  // if ss_scaled is in (0,6) look it up in the table



  if (ss_scaled >= 2 * num_integrator_stages){
    b = 0.25;
    //fprintf(stderr, "WARNING: Scaled timestep (%f) exceeds stability limit. Integrator coefficient b is set to 0.25 (VV integrator).\n", ss_scaled);
#ifdef DEBUG
    fprintf(lfp, "WARNING: Scaled timestep (%f) exceeds stability limit. b set to 0.25 (VV).\n", ss_scaled);
#endif
    fprintf(red_lfp, "WARNING: Scaled timestep (%f) exceeds stability limit. b set to 0.25 (VV).\n", ss_scaled);
  } else if (ss_scaled > 0){
    // explanation for index/ind:
    // we need to find the corresponding index in the discretized table for "hbar to b"
    // for knowing how to calculate that we need to divide the "fine"-ness of the discretization of b values (i.e. length of the array b_aia) by the value up to which we tabulated (which is atm 4 for 2s and should be 6 for 3s)
    // hbar_to_b_num_disc_steps / 4 = 1000, this is why we blow up by factor of 1000 and then round to get the index
    ind = round(ss_scaled * 1000);
    
    //safety checks
    if(ind > hbar_to_b_num_disc_steps){
      ind = hbar_to_b_num_disc_steps;
    }else if(ind == 0){
      ind = 1;
    }

    //assign b
    b = b_coeff[ind-1];

#ifdef DEBUG
    fprintf(lfp, "FindIntegrator result: %lf\n", b);
#endif

  } else{
    perror("adapt_integrator.c - There was a problem in the else-if clauses while assigning the coefficient b in the FindIntegrator function.\n");
    exit(EXIT_FAILURE);
  }

  return b;

}

double AdaptIntegrator(double ss) {

  double ss_scaled;
  double b, b_vv = 0.25;
  double rho_vv;
  struct opt_rho res;

  if (ss > 2*sqrt(2)*w_max) {
    fprintf(stderr, "adapt_integrator.c - Time step is too big.\n");
  }

  ss_scaled = scale_timestep(ss);

  rho_vv = rho_function(ss_scaled, b_vv); // Maximum value of rho for two VV steps concatenated

  res = rho_optimization_2S(ss_scaled, rho_vv);

  b = res.b;

  return b;

}



void AdaptIntegratorAndTimestep(double ss) {

#define EPS    1e-5
#define DELTA  1e-3
  double diff1, diff2;

  double b, b1, b2;
  double rho1, rho2;
  struct opt_rho results;

  // Timestep declarations
  double ss_scaled = scale_timestep(ss);
  //double ss_scaled = 2; // This line is here for testing
  //ss_scaled = 2/sqrt(w_max);

  diff1 = 1;
  //diff2 = 1;

  if (strcmp(g_inpset->integrator, "2sAIA-HMC") == 0)
    b1 = 0.211807;
  else
    b1 = 0.215109; //2sAIA-MMHMC


  b2 = 0.25;

  //find h for which rho_vv and rho_2s intersect
  rho1 = rho_function(ss_scaled, b1); // Maximun value of rho for 2-stage integrator
  rho2 = rho_function(ss_scaled, b2); // Maximun value of rho for two VV steps concatenated

  diff2 = rho2 - rho1;
  while (diff2 >= EPS) {
    ss_scaled += DELTA;
    rho1 = rho_function(ss_scaled, b1);
    rho2 = rho_function(ss_scaled, b2);
    diff2 = rho2 - rho1;
  }

#ifdef DEBUG
  fprintf(lfp, "Optimal choice of the time-step:\n");
  fprintf(lfp, "The biggest time-step is %f \n",ss_scaled);
#endif

  //rho2 = rho_function(ss_scaled, b2);
  results = rho_optimization_2S(ss_scaled, rho2);
  b = results.b;

  ////////////////////
  while (diff1 >= EPS) {
    diff1 = rho2 - results.rho;
    if (diff1 >= EPS)
      b = results.b;
    ss_scaled += DELTA;
    rho2 = rho_function(ss_scaled, b2);
    results = rho_optimization_2S(ss_scaled, rho2);
  }

  printf("Biggest time-step for which the adaptive scheme overcomes VV:\n");
  printf("The optimal parameter b is %f and the biggest time-step is %f\n", b, ss_scaled-DELTA);
#ifdef DEBUG
  fprintf(lfp,"ADAPTIVE SCHEME for the integration\n\n");
#endif

}

/**** function fnCalculateScaling
 * Description
 ** this function calculates the scaling for AIA
 ** this scaling is used to scale the stepsizes (and thus change the value of b corresponding to those stepsizes)
 ** see notes on scaling for more info

 * Arguments
 ** scaling_method - an integer from 0 to 3, specifying which scaling formula should be used (0: none, 1: SimpleMax, 2: SimpleSum, 3: Jensen)
 ** arr_frequencies - an array containing the frequencies of the system.
 *** If scaling_method is 1, only the maximum is needed and it is feasible to provide only the maximum in arr_frequencies
 ** len_arr_frequencies - and integer giving the length of the previous argument
 ** deltaH_avg - a double containing the "difference in hamiltonian averaged over the warm-up phase obtained using the Verlet integrator with L=1 and a time step $h_{\text{V}}$ which results in a high acceptance rate (>90%)"
 ** h_V - the stepsize used for deltaH_avg (see previous argument)
 ** kNumParam - the number of parameters in the system
 ****/

double fnCalculateScaling(int scaling_method, double *arr_frequencies, int len_arr_frequencies, double deltaH_avg, double h_V, int num_pars){

  //declaring
  double scaling_calc = 1.;
  int ii;

  double factor_before_root = 1.1;
  double root_numerator = 1.2;
  double root_denominator = 1.3;
  double root_term_complete = 1.4;

  double max_freq = arr_frequencies[0];
  double min_freq = arr_frequencies[0];
  double sum_freq_pow6 = 0;
  double sum_freq = 0;
  double one_div_num_pars = 1 / (double)num_pars;


  //calculate sum of frequencies and find max_freq and min_freq
  for (ii=0; ii<len_arr_frequencies; ii++){
    sum_freq += arr_frequencies[ii];
    if (arr_frequencies[ii] > max_freq){ max_freq = arr_frequencies[ii]; }
    if (arr_frequencies[ii] < min_freq){ min_freq = arr_frequencies[ii]; }
  }

#ifdef DEBUG
  fprintf(lfp, "The calculated maximum frequency is %f\n", max_freq);
  fprintf(lfp, "The calculated minimum frequency is %f\n", min_freq);
  fprintf(lfp, "The calculated sum of frequencies is %f\n", sum_freq);
#endif

  switch(scaling_method){
    case 0: //default scaling with 1 ("no scaling")

      scaling_calc = 1;

      break;
    case 141: //default scaling with sqrt(2)

      scaling_calc = sqrt(2);

      break;
    case 1: //SimpleMax

      //main parts
      factor_before_root = 2. / (h_V * max_freq);
      root_numerator = deltaH_avg;
      root_denominator = 2 * num_pars;
      root_term_complete = pow(root_numerator / root_denominator, 1/6.);

      scaling_calc = factor_before_root * root_term_complete;

      break;
    case 2: //SimpleSum

      //calculate sum of sixth-power-ed frequencies
      for (ii=0; ii<len_arr_frequencies; ii++){ sum_freq_pow6 += pow(arr_frequencies[ii], 6); }

      #ifdef DEBUG
        fprintf(lfp, "The calculated sum of sixth-power-ed frequencies is %f\n", sum_freq_pow6);
      #endif

      //main parts
      factor_before_root = 2. / h_V;
      root_numerator = deltaH_avg;
      root_denominator = 2 * sum_freq_pow6;
      root_term_complete = pow(root_numerator / root_denominator, 1/6.);

      scaling_calc = factor_before_root * root_term_complete;

      break;
    case 3: //Jensen

      //main parts
      factor_before_root = 2. / h_V;
      root_numerator = deltaH_avg;
      root_denominator = 2 * num_pars * (pow(one_div_num_pars * sum_freq, 6) + (1-one_div_num_pars) * (pow(max_freq, 6) + pow(min_freq, 6) - 2 * pow((max_freq + min_freq)/2., 6)));
      root_term_complete = pow(root_numerator / root_denominator, 1/6.);

      scaling_calc = factor_before_root * root_term_complete;

      break;
    case 11: //hard-coded for scaling method 1

      if (strcmp(g_inpset->model, "GD") == 0){
        if (strcmp(g_inpset->data, "D1000_div") == 0){
          scaling_calc = 0.663156925724192;

        } else if (strcmp(g_inpset->data, "D1000_ld_max") == 0){
          scaling_calc = 0.990464495321866;

        } else if (strcmp(g_inpset->data, "D1000_ld_max_with_mean") == 0){
          scaling_calc = 0.989436430406057;

        } else if (strcmp(g_inpset->data, "D1000_ld_mean") == 0){
          scaling_calc = 0.860398675413338;

        } else if (strcmp(g_inpset->data, "D1000_ld_mean_with_min") == 0){
          scaling_calc = 0.849496182340099;

        } else if (strcmp(g_inpset->data, "D1000_ld_max_with_min") == 0){
          scaling_calc = 1.02118401099791;

        } else if (strcmp(g_inpset->data, "D1000_ld_min") == 0){
          scaling_calc = 0.614304702478336;

        } else if (strcmp(g_inpset->data, "D1000_all_1") == 0){
          scaling_calc = 1.00091485200786;

        } else if (strcmp(g_inpset->data, "D1000_unif_10_50") == 0){
          scaling_calc = 0.821493541160245;

        } else if (strcmp(g_inpset->data, "D1000_all_00001") == 0){
          scaling_calc = 0.997150535745051;

        } else if (strcmp(g_inpset->data, "D1000_all_1_one_4000") == 0){
          scaling_calc = 0.31567323362637;

        } else if (strcmp(g_inpset->data, "D1000_unif_00001_00010") == 0){
          scaling_calc = 0.845099358861782;

        } else if (strcmp(g_inpset->data, "D1000_norm_1_01_990_norm_4000_40_10") == 0){
          scaling_calc = 0.460861960716816;

        } else if (strcmp(g_inpset->data, "D1000_norm_00001_000001") == 0){
          scaling_calc = 0.917464511676275;

        } else if (strcmp(g_inpset->data, "D1000_norm_4000_40_990_norm_1_01_10") == 0){
          scaling_calc = 0.984219869792464;

        } else if (strcmp(g_inpset->data, "D2000_div") == 0){
          scaling_calc = 0.660917416301992;

        } else if (strcmp(g_inpset->data, "D2000_norm_2_02_1980_norm_8000_80_20") == 0){
          scaling_calc = 0.462624182267821;

        } else if (strcmp(g_inpset->data, "D2000_unif_00001_0001") == 0){
          scaling_calc = 0.873312415612729;

        } else if (strcmp(g_inpset->data, "D2000_all_05") == 0){
          scaling_calc = 1.02230249525356;

        } else {
          fnPrintErrorDatasetNotFound(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);
        }
      } else if (strcmp(g_inpset->model, "BLR") == 0){
      	if (strcmp(g_inpset->data, "german") == 0){
      		scaling_calc = 1.06125564283072;

      	} else if (strcmp(g_inpset->data, "heart") == 0){
      		scaling_calc = 1.10971968810518;

      	} else if (strcmp(g_inpset->data, "musk") == 0){
          scaling_calc = 2.37702227789176;

        } else if (strcmp(g_inpset->data, "german_dummy") == 0){
          scaling_calc = 1.22643539281963;

      	} else {
          fnPrintErrorDatasetNotFound(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);
        }
      } else {
        fnPrintErrorModelNotFound(scaling_method, g_inpset->model, g_inpset->data);
        exit(EXIT_FAILURE);
      } //end of if else if for "model"

      break;
    case 12: //hard coded for scaling method 2

      if (strcmp(g_inpset->model, "GD") == 0){
        if (strcmp(g_inpset->data, "D1000_div") == 0){
          scaling_calc = 1.01009367981544;

        } else if (strcmp(g_inpset->data, "D1000_ld_max") == 0){
          scaling_calc = 0.999857248451614;

        } else if (strcmp(g_inpset->data, "D1000_ld_max_with_min") == 0){
          scaling_calc = 0.999516558554743;

        } else if (strcmp(g_inpset->data, "D1000_ld_mean") == 0){
          scaling_calc = 1.01282921059062;

        } else if (strcmp(g_inpset->data, "D1000_ld_mean_with_min") == 0){
          scaling_calc = 1.00028673591488;

        } else if (strcmp(g_inpset->data, "D1000_ld_min") == 0){
          scaling_calc = 1.07787509412335;

        } else if (strcmp(g_inpset->data, "D1000_all_1") == 0){
          scaling_calc = 1.00091485200786;

        } else if (strcmp(g_inpset->data, "D1000_unif_10_50") == 0){
          scaling_calc = 0.995984323075219;

        } else if (strcmp(g_inpset->data, "D1000_all_00001") == 0){
          scaling_calc = 0.997150535745051;

        } else if (strcmp(g_inpset->data, "D1000_all_1_one_4000") == 0){
          scaling_calc = 0.998246412012785;

        } else if (strcmp(g_inpset->data, "D1000_unif_00001_00010") == 0){
          scaling_calc = 1.03422491642546;

        } else if (strcmp(g_inpset->data, "D1000_norm_1_01_990_norm_4000_40_10") == 0){
          scaling_calc = 0.999432071888935;

        } else if (strcmp(g_inpset->data, "D1000_norm_00001_000001") == 0){
          scaling_calc = 1.0469166003233;

        } else if (strcmp(g_inpset->data, "D1000_norm_4000_40_990_norm_1_01_10") == 0){
          scaling_calc = 1.00043563839906;

        } else if (strcmp(g_inpset->data, "D2000_div") == 0){
          scaling_calc = 1.00722337745805;

        } else if (strcmp(g_inpset->data, "D2000_norm_2_02_1980_norm_8000_80_20") == 0){
          scaling_calc = 1.00568343444333;

        } else if (strcmp(g_inpset->data, "D2000_unif_00001_0001") == 0){
          scaling_calc = 1.0856636633099;

        } else if (strcmp(g_inpset->data, "D2000_all_05") == 0){
          scaling_calc = 1.02230249525356;

        } else {
          fnPrintErrorDatasetNotFound(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);
        }
      } else if (strcmp(g_inpset->model, "BLR") == 0){
      	if (strcmp(g_inpset->data, "german") == 0){
      		scaling_calc = 1.14152181584973;

      	} else if (strcmp(g_inpset->data, "heart") == 0){
          scaling_calc = 1.19256727853197;

        } else if (strcmp(g_inpset->data, "musk") == 0){
          scaling_calc = 3.42934032483101;

        } else if (strcmp(g_inpset->data, "german_dummy") == 0){
          scaling_calc = 1.38901525970743;

      	} else {
          fnPrintErrorDatasetNotFound(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);
        }
      } else {
        fnPrintErrorModelNotFound(scaling_method, g_inpset->model, g_inpset->data);
        exit(EXIT_FAILURE);
      } //end of if else if for "model"

      break;
    case 13: //hard coded for scaling method 3

      if (strcmp(g_inpset->model, "GD") == 0){
        if (strcmp(g_inpset->data, "D1000_div") == 0){
          scaling_calc = 0.666108699824548;

        } else if (strcmp(g_inpset->data, "D1000_ld_max") == 0){
          scaling_calc = 0.997529249586737;

        } else if (strcmp(g_inpset->data, "D1000_ld_max_with_min") == 0){
          scaling_calc = 0.890239781227408;

        } else if (strcmp(g_inpset->data, "D1000_ld_mean") == 0){
          scaling_calc = 0.904798472839493;

        } else if (strcmp(g_inpset->data, "D1000_ld_mean_with_min") == 0){
          scaling_calc = 0.810967148573336;

        } else if (strcmp(g_inpset->data, "D1000_ld_min") == 0){
          scaling_calc = 0.617002417725517;

        } else if (strcmp(g_inpset->data, "D1000_all_1") == 0){
          scaling_calc = 1.00091485200786;

        } else if (strcmp(g_inpset->data, "D1000_unif_10_50") == 0){
          scaling_calc = 0.833382934158716;

        } else if (strcmp(g_inpset->data, "D1000_all_00001") == 0){
          scaling_calc = 0.997150535745051;

        } else if (strcmp(g_inpset->data, "D1000_all_1_one_4000") == 0){
          scaling_calc = 0.317569689977036;

        } else if (strcmp(g_inpset->data, "D1000_unif_00001_00010") == 0){
          scaling_calc = 0.846592325102367;

        } else if (strcmp(g_inpset->data, "D1000_norm_1_01_990_norm_4000_40_10") == 0){
          scaling_calc = 0.463586156800792;

        } else if (strcmp(g_inpset->data, "D1000_norm_00001_000001") == 0){
          scaling_calc = 0.963446670873037;

        } else if (strcmp(g_inpset->data, "D1000_norm_4000_40_990_norm_1_01_10") == 0){
          scaling_calc = 0.890087489421137;

        } else if (strcmp(g_inpset->data, "D2000_div") == 0){
          scaling_calc = 0.663805619950787;

        } else if (strcmp(g_inpset->data, "D2000_norm_2_02_1980_norm_8000_80_20") == 0){
          scaling_calc = 0.465316794975458;

        } else if (strcmp(g_inpset->data, "D2000_unif_00001_0001") == 0){
          scaling_calc = 0.877233718080147;

        } else if (strcmp(g_inpset->data, "D2000_all_05") == 0){
          scaling_calc = 1.02230249525356;

        } else {
          fnPrintErrorDatasetNotFound(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);
        }
      } else if (strcmp(g_inpset->model, "BLR") == 0){
      	if (strcmp(g_inpset->data, "german") == 0){
      		scaling_calc = 1.05965745103915;

      	} else if (strcmp(g_inpset->data, "heart") == 0){
          scaling_calc = 1.15126840275231;

        } else if (strcmp(g_inpset->data, "musk") == 0){
          scaling_calc = 2.38002466487451;

        } else if (strcmp(g_inpset->data, "german_dummy") == 0){
          scaling_calc = 1.21045757549248;

      	} else {
          fnPrintErrorDatasetNotFound(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);
        }
      } else {
        fnPrintErrorModelNotFound(scaling_method, g_inpset->model, g_inpset->data);
        exit(EXIT_FAILURE);
      } //end of if else if for "model"

      break;
    case 21: //hard-coded for scaling method using -logAR instead of <deltaH>

      if (strcmp(g_inpset->model, "GD") == 0){
        if (strcmp(g_inpset->data, "D1000_div") == 0){
          scaling_calc = 1.01125655351603;

        } else if (strcmp(g_inpset->data, "D1000_ld_max") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else if (strcmp(g_inpset->data, "D1000_ld_max_with_mean") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else if (strcmp(g_inpset->data, "D1000_ld_mean") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else if (strcmp(g_inpset->data, "D1000_ld_mean_with_min") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else if (strcmp(g_inpset->data, "D1000_ld_max_with_min") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else if (strcmp(g_inpset->data, "D1000_ld_min") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else if (strcmp(g_inpset->data, "D1000_all_1") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else if (strcmp(g_inpset->data, "D1000_unif_10_50") == 0){
          scaling_calc = 1.00351445393173;

        } else if (strcmp(g_inpset->data, "D1000_all_00001") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else if (strcmp(g_inpset->data, "D1000_all_1_one_4000") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else if (strcmp(g_inpset->data, "D1000_unif_00001_00010") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else if (strcmp(g_inpset->data, "D1000_norm_1_01_990_norm_4000_40_10") == 0){
          scaling_calc = 1.02037285415300;

        } else if (strcmp(g_inpset->data, "D1000_norm_00001_000001") == 0){
          scaling_calc = 1.00953545830899;

        } else if (strcmp(g_inpset->data, "D1000_norm_4000_40_990_norm_1_01_10") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else if (strcmp(g_inpset->data, "D2000_div") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else if (strcmp(g_inpset->data, "D2000_norm_2_02_1980_norm_8000_80_20") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else if (strcmp(g_inpset->data, "D2000_unif_00001_0001") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else if (strcmp(g_inpset->data, "D2000_all_05") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else {
          fnPrintErrorDatasetNotFound(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);
        }
      } else if (strcmp(g_inpset->model, "BLR") == 0){
        if (strcmp(g_inpset->data, "german") == 0){
          scaling_calc = 1.6079;

        } else if (strcmp(g_inpset->data, "heart") == 0){
          fnPrintErrorScalingNotYetImplemented(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);

        } else if (strcmp(g_inpset->data, "musk") == 0){
          scaling_calc = 6.1244;

        } else {
          fnPrintErrorDatasetNotFound(scaling_method, g_inpset->model, g_inpset->data);
          exit(EXIT_FAILURE);
        }
      } else {
        fnPrintErrorModelNotFound(scaling_method, g_inpset->model, g_inpset->data);
        exit(EXIT_FAILURE);
      } //end of if else if for "model"

      break;
    default:

      fprintf(stderr, "adapt_integrator.c - For using scaling, the parameter scaling_method has to be 0, 1, 2, 3, 11, 12, 13 or 21, but it is %i\n", scaling_method);
      exit(EXIT_FAILURE);

      break;
  } //end switch scaling_method

  //Print info
#ifdef DEBUG
  fprintf(lfp, "-- fnCalculateScaling printing\nscaling_method %i\ndeltaH_avg %.10f\nh_V %.6f\nnum_pars %i\nscaling %lf\n-- end of fnCalculateScaling printing\n", scaling_method, deltaH_avg, h_V, num_pars, scaling_calc);
#endif
  fprintf(red_lfp, "-- fnCalculateScaling printing\nscaling_method %i\ndeltaH_avg %.10f\nh_V %.6f\nnum_pars %i\nscaling %lf\n-- end of fnCalculateScaling printing\n", scaling_method, deltaH_avg, h_V, num_pars, scaling_calc);

  return scaling_calc;

} //end of function fnCalculateScaling
